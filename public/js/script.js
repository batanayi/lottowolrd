$(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 80) {
        $("header").addClass("raised");
        $("header").removeClass("transparent");
    } else {
        $("header").addClass("transparent");
        $("header").removeClass("raised");
    }
});

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            300: {
                items: 2,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: false
            }
        }
    });
});