@extends('layouts.app', ['is_auth' => true])

@section('content')
    <div _ngcontent-c0="" class="content-wrapper header-static without-header">
        <app-auth _nghost-c54="" class="ng-star-inserted">
            @include('auth.close')
            <router-outlet _ngcontent-c54=""></router-outlet>
            <app-sign-up _nghost-c60="" class="ng-star-inserted">
                <app-auth-container _ngcontent-c60="" title="Sign Up">
                    <div class="auth-content"><!---->
                        <div class="title font-xl text-center d-none d-md-block ng-star-inserted">Reset Password</div>
                        <div class="auth-card">
                            <div class="auth-card-wrapp">
                                <div _ngcontent-c60="" card="">
                                    <form method="POST" _ngcontent-c60="" id="signupForm" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="ng-invalid ng-dirty ng-touched">
                                        @csrf
                                        <input type="checkbox" style="display: none;" name="remember" id="remember" checked>
                                        <div _ngcontent-c60="" class="auth-card-padding">
                                            <div _ngcontent-c47="" class="form-title text-center font-m ng-star-inserted"><span _ngcontent-c47="">We’ll send you new password</span></div>
                                            @if ($errors->has('email') || $errors->has('password'))
                                                <div _ngcontent-c24="" class="form-error text-center font-m warning-text ng-star-inserted">
                                                    <!---->
                                                    <span _ngcontent-c24="" class="ng-star-inserted">Wrong Email</span>
                                                    <!---->
                                                </div>
                                            @endif
                                            <mat-form-field _ngcontent-c6="" class="w-100 mat-form-field ng-tns-c10-12 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-hide-placeholder mat-form-field-should-float ng-dirty ng-valid ng-touched mat-form-field-autofilled{{ $errors->has('email') ? ' mat-form-field-invalid ng-invalid' : '' }}" id="usernameValue">
                                                <div class="mat-form-field-wrapper">
                                                    <div class="mat-form-field-flex">
                                                        <!---->
                                                        <!---->
                                                        <div class="mat-form-field-infix">
                                                            <input _ngcontent-c6="" autocomplete="email" class="mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-dirty ng-valid ng-touched cdk-text-field-autofilled" formcontrolname="username" id="usernameInput" name="email" value="{{ old('email') }}" matinput="" type="email" aria-invalid="false" aria-required="false">
                                                            <span class="mat-form-field-label-wrapper">
                                                                <!---->
                                                                <label class="mat-form-field-label ng-tns-c10-12 ng-star-inserted" id="mat-form-field-label-25" for="usernameInput" aria-owns="usernameInput">
                                                                    <!---->
                                                                    <!---->
                                                                    <mat-placeholder _ngcontent-c6="" class="ng-star-inserted">
                                                                        <app-rb-input-errors _ngcontent-c6=""><span>Email</span></app-rb-input-errors>
                                                                    </mat-placeholder>
                                                                    <!---->
                                                                    <!---->
                                                                 </label>
                                                            </span>
                                                        </div>
                                                        <!---->
                                                    </div>
                                                    <!---->
                                                    <div class="mat-form-field-underline ng-tns-c10-12 ng-star-inserted"><span class="mat-form-field-ripple"></span></div>
                                                    <div class="mat-form-field-subscript-wrapper">
                                                        <!---->
                                                        <!---->
                                                        <div class="mat-form-field-hint-wrapper ng-tns-c10-12 ng-trigger ng-trigger-transitionMessages ng-star-inserted" style="opacity: 1; transform: translateY(0%);">
                                                            <!---->
                                                            <div class="mat-form-field-hint-spacer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </mat-form-field>
                                        </div>
                                        <button type="submit" _ngcontent-c60="" class="rb-btn rb-btn-default rb-btn-purple w-100 form-button rb-btn-grey" id="signUpBtn"><span _ngcontent-c60="">Send Password</span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <app-auth-sub-footer _ngcontent-c6="" subfooter="" _nghost-c58=""> Back to <a _ngcontent-c6="" href="{{ route('login') }}"> Log In </a></app-auth-sub-footer>
                    </div>
                    <app-auth-footer _ngcontent-c6="" footer="" _nghost-c12="">
                        <!---->
                        <div _ngcontent-c12="" class="ng-star-inserted">
                            <ul _ngcontent-c12="">
                                <li _ngcontent-c12=""><a _ngcontent-c12="" href="/privacy" target="_blank">Security and Privacy</a></li>
                                <li _ngcontent-c12=""><span _ngcontent-c12="">|</span></li>
                                <li _ngcontent-c12=""><a _ngcontent-c12="" href="/terms" target="_blank">Terms of Use</a></li>
                            </ul>
                        </div>
                        <!---->
                    </app-auth-footer>
                </app-auth-container>
            </app-sign-up>
        </app-auth>
    </div>
@endsection
