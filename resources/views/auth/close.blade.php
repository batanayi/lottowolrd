<app-rb-close-btn _ngcontent-c54="" _nghost-c55="">
    <a _ngcontent-c55="" class="close modal__close-btn" href="{{ route('home') }}">
        <svg _ngcontent-c55="" height="16px" viewBox="0 0 16 16" width="16px">
            <g _ngcontent-c55="" fill="none" fill-rule="evenodd" id="Symbols" stroke="none"
               stroke-width="1">
                <g _ngcontent-c55="" id="Button-Close" transform="translate(-17.000000, -17.000000)">
                    <g _ngcontent-c55="" id="Group-2" transform="translate(15.000000, 15.000000)">
                        <g _ngcontent-c55="" id="Group" stroke="#ABABAB" stroke-linecap="round"
                           stroke-width="2" transform="translate(3.000000, 3.000000)">
                            <path _ngcontent-c55="" d="M0,0 L14,14" id="Path-3"></path>
                            <path _ngcontent-c55="" d="M0,0 L14,14" id="Path-3-Copy"
                                  transform="translate(7.000000, 7.000000) scale(-1, 1) translate(-7.000000, -7.000000) "></path>
                        </g>
                        <rect _ngcontent-c55="" height="20" id="Rectangle-16" width="20" x="0" y="0"></rect>
                    </g>
                </g>
            </g>
        </svg>
    </a>
</app-rb-close-btn>