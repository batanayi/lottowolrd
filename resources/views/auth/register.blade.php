@extends('layouts.app', ['is_auth' => true])

@section('content')
    <div _ngcontent-c0="" class="content-wrapper header-static without-header">
        <app-auth _nghost-c54="" class="ng-star-inserted">
            @include('auth.close')
            <app-sign-up _nghost-c60="" class="ng-star-inserted">
                <app-auth-container _ngcontent-c60="" title="Sign Up">
                    <div class="auth-content"><!---->
                        <div class="title font-xl text-center d-none d-md-block ng-star-inserted"> Sign Up</div>
                        <div class="auth-card">
                            <div class="auth-card-wrapp">
                                <div _ngcontent-c60="" card="">
                                    <form _ngcontent-c60="" id="signupForm" novalidate=""
                                          class="ng-invalid ng-dirty ng-touched">
                                        <div _ngcontent-c60="" class="fake-autofill-fields"><input _ngcontent-c60=""
                                                                                                   name="fakeusernameremembered"
                                                                                                   tabindex="-1"
                                                                                                   type="text"><input
                                                    _ngcontent-c60="" name="fakepasswordremembered" tabindex="-1"
                                                    type="password"></div>
                                        <div _ngcontent-c60="" class="auth-card-padding">
                                            <div _ngcontent-c60="" class="w-100 o-auth">
                                                <app-fb-login-btn _ngcontent-c60="" _nghost-c46=""><i _ngcontent-c46=""
                                                                                                      class="fb-icon"></i>
                                                    <p _ngcontent-c46=""> Sign up with Facebook
                                                    </p></app-fb-login-btn>
                                                <app-rb-middle-line _ngcontent-c60="" _nghost-c18="">
                                                    <div _ngcontent-c18=""
                                                         class="row align-items-center text-type-grey">
                                                        <div _ngcontent-c18="" class="col-md-4 offset-md-0">
                                                            <div _ngcontent-c18="" class="middle-line"></div>
                                                        </div>
                                                        <div _ngcontent-c18="" class="col-md-4 offset-md-0">
                                                            <div _ngcontent-c18="" class="text"><p _ngcontent-c60=""
                                                                                                   class="font-xs text-center text-grey">
                                                                    or with email</p></div>
                                                        </div>
                                                        <div _ngcontent-c18="" class="col-md-4 offset-md-0">
                                                            <div _ngcontent-c18="" class="middle-line"></div>
                                                        </div>
                                                    </div>
                                                </app-rb-middle-line>
                                            </div>
                                            <mat-form-field _ngcontent-c60=""
                                                            class="w-100 mat-form-field ng-tns-c43-41 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-hide-placeholder ng-untouched ng-pristine ng-invalid">
                                                <div class="mat-form-field-wrapper">
                                                    <div class="mat-form-field-flex"><!----><!---->
                                                        <div class="mat-form-field-infix"><input _ngcontent-c60=""
                                                                                                 autocomplete="given-name"
                                                                                                 class="mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-untouched ng-pristine ng-invalid"
                                                                                                 formcontrolname="nameFirst"
                                                                                                 id="nameFirstInput"
                                                                                                 matinput=""
                                                                                                 name="nameFirst"
                                                                                                 aria-invalid="false"
                                                                                                 aria-required="false"><span
                                                                    class="mat-form-field-label-wrapper"><!----><label
                                                                        class="mat-form-field-label ng-tns-c43-41 mat-empty mat-form-field-empty ng-star-inserted"
                                                                        id="mat-form-field-label-65"
                                                                        for="nameFirstInput" aria-owns="nameFirstInput"><!---->
                                                                    <!----><mat-placeholder _ngcontent-c60=""
                                                                                            class="ng-star-inserted"><app-rb-input-errors
                                                                                _ngcontent-c60=""><span>First Name</span></app-rb-input-errors></mat-placeholder>
                                                                    <!----><!----></label></span></div><!----></div>
                                                    <!---->
                                                    <div class="mat-form-field-underline ng-tns-c43-41 ng-star-inserted">
                                                        <span class="mat-form-field-ripple"></span></div>
                                                    <div class="mat-form-field-subscript-wrapper"><!----><!---->
                                                        <div class="mat-form-field-hint-wrapper ng-tns-c43-41 ng-trigger ng-trigger-transitionMessages ng-star-inserted"
                                                             style="opacity: 1; transform: translateY(0%);"><!---->
                                                            <div class="mat-form-field-hint-spacer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </mat-form-field>
                                            <mat-form-field _ngcontent-c60=""
                                                            class="w-100 mat-form-field ng-tns-c43-42 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-hide-placeholder ng-untouched ng-pristine ng-invalid">
                                                <div class="mat-form-field-wrapper">
                                                    <div class="mat-form-field-flex"><!----><!---->
                                                        <div class="mat-form-field-infix"><input _ngcontent-c60=""
                                                                                                 autocomplete="family-name"
                                                                                                 class="mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-untouched ng-pristine ng-invalid"
                                                                                                 formcontrolname="nameLast"
                                                                                                 id="nameLastInput"
                                                                                                 matinput=""
                                                                                                 name="nameLast"
                                                                                                 aria-invalid="false"
                                                                                                 aria-required="false"><span
                                                                    class="mat-form-field-label-wrapper"><!----><label
                                                                        class="mat-form-field-label ng-tns-c43-42 mat-empty mat-form-field-empty ng-star-inserted"
                                                                        id="mat-form-field-label-67" for="nameLastInput"
                                                                        aria-owns="nameLastInput"><!----><!----><mat-placeholder
                                                                            _ngcontent-c60="" class="ng-star-inserted"><app-rb-input-errors
                                                                                _ngcontent-c60=""><span>Last Name</span></app-rb-input-errors></mat-placeholder>
                                                                    <!----><!----></label></span></div><!----></div>
                                                    <!---->
                                                    <div class="mat-form-field-underline ng-tns-c43-42 ng-star-inserted">
                                                        <span class="mat-form-field-ripple"></span></div>
                                                    <div class="mat-form-field-subscript-wrapper"><!----><!---->
                                                        <div class="mat-form-field-hint-wrapper ng-tns-c43-42 ng-trigger ng-trigger-transitionMessages ng-star-inserted"
                                                             style="opacity: 1; transform: translateY(0%);"><!---->
                                                            <div class="mat-form-field-hint-spacer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </mat-form-field>
                                            <mat-form-field _ngcontent-c60=""
                                                            class="w-100 mat-form-field ng-tns-c43-43 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-hide-placeholder ng-invalid mat-form-field-should-float ng-dirty mat-form-field-invalid ng-touched mat-form-field-autofilled">
                                                <div class="mat-form-field-wrapper">
                                                    <div class="mat-form-field-flex"><!----><!---->
                                                        <div class="mat-form-field-infix"><input _ngcontent-c60=""
                                                                                                 autocomplete="email"
                                                                                                 class="mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-invalid ng-dirty ng-touched cdk-text-field-autofilled"
                                                                                                 formcontrolname="username"
                                                                                                 id="email" matinput=""
                                                                                                 name="email"
                                                                                                 aria-invalid="true"
                                                                                                 aria-required="false"><span
                                                                    class="mat-form-field-label-wrapper"><!----><label
                                                                        class="mat-form-field-label ng-tns-c43-43 ng-star-inserted"
                                                                        id="mat-form-field-label-69" for="email"
                                                                        aria-owns="email"><!----><!----><mat-placeholder
                                                                            _ngcontent-c60="" class="ng-star-inserted"><app-rb-input-errors
                                                                                _ngcontent-c60=""><span>Invalid Email</span></app-rb-input-errors></mat-placeholder>
                                                                    <!----><!----></label></span></div><!----></div>
                                                    <!---->
                                                    <div class="mat-form-field-underline ng-tns-c43-43 ng-star-inserted">
                                                        <span class="mat-form-field-ripple"></span></div>
                                                    <div class="mat-form-field-subscript-wrapper"><!----><!---->
                                                        <div class="mat-form-field-hint-wrapper ng-tns-c43-43 ng-trigger ng-trigger-transitionMessages ng-star-inserted"
                                                             style="opacity: 1; transform: translateY(0%);"><!---->
                                                            <div class="mat-form-field-hint-spacer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </mat-form-field>
                                            <mat-form-field _ngcontent-c60=""
                                                            class="w-100 mat-form-field ng-tns-c43-44 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-hide-placeholder ng-untouched ng-pristine ng-invalid">
                                                <div class="mat-form-field-wrapper">
                                                    <div class="mat-form-field-flex"><!----><!---->
                                                        <div class="mat-form-field-infix"><input _ngcontent-c60=""
                                                                                                 autocomplete="off"
                                                                                                 class="mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-untouched ng-pristine ng-invalid"
                                                                                                 formcontrolname="plainPassword"
                                                                                                 id="password"
                                                                                                 matinput=""
                                                                                                 name="password"
                                                                                                 type="password"
                                                                                                 aria-invalid="false"
                                                                                                 aria-required="false"><span
                                                                    class="mat-form-field-label-wrapper"><!----><label
                                                                        class="mat-form-field-label ng-tns-c43-44 mat-empty mat-form-field-empty ng-star-inserted"
                                                                        id="mat-form-field-label-71" for="password"
                                                                        aria-owns="password"><!----><!----><mat-placeholder
                                                                            _ngcontent-c60="" class="ng-star-inserted"><app-rb-input-errors
                                                                                _ngcontent-c60=""><span>Password</span></app-rb-input-errors></mat-placeholder>
                                                                    <!----><!----></label></span></div><!----></div>
                                                    <!---->
                                                    <div class="mat-form-field-underline ng-tns-c43-44 ng-star-inserted">
                                                        <span class="mat-form-field-ripple"></span></div>
                                                    <div class="mat-form-field-subscript-wrapper"><!----><!---->
                                                        <div class="mat-form-field-hint-wrapper ng-tns-c43-44 ng-trigger ng-trigger-transitionMessages ng-star-inserted"
                                                             style="opacity: 1; transform: translateY(0%);"><!---->
                                                            <div class="mat-form-field-hint-spacer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </mat-form-field>
                                        </div>
                                        <button _ngcontent-c60=""
                                                class="rb-btn rb-btn-default rb-btn-purple w-100 form-button rb-btn-grey"
                                                id="signUpBtn"><span _ngcontent-c60="">Sign Up</span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <app-auth-sub-footer _ngcontent-c60="" subfooter="" _nghost-c58=""> Already have Rentrand
                            account? <a _ngcontent-c60="" href="{{ route('login') }}">Log in</a></app-auth-sub-footer>
                    </div>
                    <app-auth-footer _ngcontent-c60="" footer="" _nghost-c59=""><!---->
                        <div _ngcontent-c59="" class="ng-star-inserted"> By signing up you agree to Rentrand’s <a
                                    _ngcontent-c59="" href="/terms" target="_blank">Terms of Use</a> and <a
                                    _ngcontent-c59="" href="/privacy" target="_blank">Privacy Policy</a></div><!---->
                    </app-auth-footer>
                </app-auth-container>
            </app-sign-up>
        </app-auth>
    </div>
@endsection