<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameDraw extends Model
{
    //

    /**
     * Get all of the days for the game type.
     */
    public function gameday()
    {
        return $this->belongsTo('App\GameDay');
    }
}
