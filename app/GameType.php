<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class GameType extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get all of the draws for the game type.
     */
    public function draws()
    {
        return $this->hasMany('App\GameDraw');
    }

    /**
     * Get all of the days for the game type.
     */
    public function gamedays()
    {
        return $this->belongsToMany('App\GameDay', 'game_type_days', 'game_type_id', 'game_day_id');
    }
}
