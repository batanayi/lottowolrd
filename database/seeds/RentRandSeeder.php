<?php

use Illuminate\Database\Seeder;
use App\Amenity;

class RentRandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    }

    public function setAgency($externalUrl){
        $crawler = Goutte::request('GET', $externalUrl);

        $node = $crawler->filter('.p24_results .container');

        $nameNode = $node->filter('h1');
        $addressNode = $node->filter('#google-map img');

        if ($nameNode->count() > 0) {
            $attributes = [
                'name' => $nameNode->count() ? $nameNode->text() : '',
                'address' => $addressNode->count() ? $addressNode->attr('alt') : '',
            ];
            $values = [
                'active' => true,
                'external_url' => $externalUrl
            ];

            return \App\Agency::updateOrCreate($attributes, $values);
        }

        return false;
    }
}
