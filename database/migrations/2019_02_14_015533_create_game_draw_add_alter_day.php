<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameDrawAddAlterDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_draws', function (Blueprint $table) {
            $table->dropColumn('game_day_id');
        });

        Schema::table('game_draws', function (Blueprint $table) {
            $table->unsignedInteger('game_day_id')->index();
            $table->foreign('game_day_id')->references('id')->on('game_days');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_draws', function (Blueprint $table) {
            //
        });
    }
}
