<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameDraws extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_draws', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->decimal('rollover')->nullable();
            $table->decimal('jackpot')->nullable();
            $table->integer('game_day_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_draws');
    }
}
