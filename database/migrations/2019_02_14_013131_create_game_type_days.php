<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTypeDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_type_days', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_day_id')->index();
            $table->unsignedInteger('game_type_id')->index();
            $table->timestamps();

            $table->foreign('game_day_id')->references('id')->on('game_days');
            $table->foreign('game_type_id')->references('id')->on('game_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_type_days');
    }
}
